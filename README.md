# Helm Template Infinity css
Helm Template that creates a Deployment, Service, HPA and an Ingress

## Requirements
Helm v3.0

## Getting Started
- Modify the `values.yaml` file with the appropriate values

## Run Application


```bash
  helm install <release name> -n <namespace> .
```
